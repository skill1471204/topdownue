// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownUEGameMode.h"
#include "TopDownUEPlayerController.h"
#include "TopDownUE/Character/TopDownUECharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDownUEGameMode::ATopDownUEGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownUEPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}