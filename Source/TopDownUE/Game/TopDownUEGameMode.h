// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopDownUEGameMode.generated.h"

UCLASS(minimalapi)
class ATopDownUEGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDownUEGameMode();
};



