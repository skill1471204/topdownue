// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownUE.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDownUE, "TopDownUE" );

DEFINE_LOG_CATEGORY(LogTopDownUE)
 